import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Button, Container, Stack } from "@mui/material/";
import Zad2 from "./tasks/Zad2";
import Zad3 from "./tasks/Zad3";

export default function App() {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/zad2">
          <Zad2 />
        </Route>
        <Route path="/zad3">
          <Zad3 />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

function Home() {
  return (
    <React.Fragment>
      <Container
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: "100vh",
          width: "100%",
        }}
      >
        <Stack spacing={3} direction="column">
          <Link to="/zad2" style={{ textDecoration: "none" }}>
            <Button variant="contained" size="large">
              Zadanie 2
            </Button>
          </Link>
          <Link to="/zad3" style={{ textDecoration: "none" }}>
            <Button variant="contained" size="large">
              Zadanie 3
            </Button>
          </Link>
        </Stack>
      </Container>
    </React.Fragment>
  );
}
