import * as React from "react";
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  IconButton,
  Switch,
  FormControlLabel,
  FormGroup,
  MenuItem,
  Menu,
  Container,
  Button,
} from "@mui/material";
import { AccountCircle } from "@mui/icons-material/";
import NotificationsIcon from "@mui/icons-material/Notifications";
import SearchIcon from "@mui/icons-material/Search";
import { styled } from "@mui/material/styles";
import logo from "../img/logo.png";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText("#9e1c1c"),
  backgroundColor: "#9e1c1c",
  borderRadius: 20,
  marginLeft: "20px",
  "&:hover": {
    backgroundColor: "#9e1c1ca8",
  },
}));

//config for a few IconButton elements
const IconButtonCustome = (html) => {
  return (
    <IconButton
      size="large"
      aria-label="account of current user"
      aria-controls="menu-appbar"
      aria-haspopup="true"
      color="inherit"
    >
      {html}
    </IconButton>
  );
};

export default function MenuAppBar() {
  const [auth, setAuth] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Container>
      <Box sx={{ flexGrow: 1 }} style={{ color: "white" }}>
        <FormGroup>
          <FormControlLabel
            control={
              <Switch
                checked={auth}
                onChange={handleChange}
                aria-label="login switch"
              />
            }
            label={auth ? "Logout" : "Login"}
          />
        </FormGroup>
        <AppBar
          position="static"
          style={{ backgroundColor: "white", color: "black" }}
        >
          <Toolbar>
            <img
              src={logo}
              style={{ width: "40px", height: "40px" }}
              alt="logo"
            />

            <Typography component="div" sx={{ m: 2, cursor: "pointer" }}>
              Wydarzenia
            </Typography>
            <Typography component="div" sx={{ m: 2, cursor: "pointer" }}>
              Kontakt
            </Typography>
            <Typography
              component="div"
              sx={{ m: 2, flexGrow: 1, cursor: "pointer" }}
            >
              Wesprzyj Nas
            </Typography>
            {IconButtonCustome(<SearchIcon sx={{ m: 1 }} />)}
            {auth ? (
              ""
            ) : (
              <ColorButton onClick={() => setAuth(true)} variant="contained">
                Zaloguj się
              </ColorButton>
            )}
            {auth && (
              <div>
                {IconButtonCustome(<NotificationsIcon sx={{ m: 1 }} />)}
                {IconButtonCustome(
                  <AccountCircle sx={{ m: 1 }} onClick={(e) => handleMenu(e)} />
                )}
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <MenuItem onClick={handleClose}>Profile</MenuItem>
                  <MenuItem onClick={handleClose}>My account</MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </Box>
    </Container>
  );
}
