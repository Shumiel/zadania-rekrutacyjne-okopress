import React, { useEffect, useState } from "react";
import {
  TextField,
  Container,
  Box,
  Card,
  CardContent,
  Button,
  Typography,
  CircularProgress,
} from "@mui/material/";

/*  >>>>>>>>>>>>>>>>>>>>>>> TOKEN GOES HERE! <<<<<<<<<<<<<<<<<<<<<<<<< */
const token = "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";
/*  >>>>>>>>>>>>>>>>>>>>>>> TOKEN GOES HERE! <<<<<<<<<<<<<<<<<<<<<<<<< */

const Zad3 = () => {
  const [search, setSearch] = useState("");
  const [records, setRecords] = useState([]);
  const [isSearching, setisSearching] = useState(false);
  //const after = ', after: "Y3Vyc29yOjEw"'; // for infinite scroll

  useEffect(() => {
    if (search.length >= 3) {
      setisSearching(true);
      fetch("https://api.github.com/graphql", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "bearer " + token,
        },
        body: JSON.stringify({
          query: `{
            search(query: "${search}", type: REPOSITORY, first: 10) {
              repositoryCount
              edges {
                node {
                  ... on Repository {
                    name
                    id
                    url
                  }
                }
              }
              pageInfo {
                endCursor
                hasNextPage
                startCursor
                hasPreviousPage
              }
            }
          }`,
        }),
      })
        .then((res) => res.json())
        .then((data) => setRecords(data.data.search.edges))
        .then(() => setisSearching(false))
        .catch((err) => console.log(err));
    } else {
      setRecords([]);
    }
  }, [search]);

  const handleChange = (e) => {
    setSearch(e.target.value);
    console.log(records);
  };

  return (
    <Container maxWidth="sm">
      <Box style={{ backgroundColor: "#fff", opacity: 0.7, margin: 5 }}>
        <TextField
          label="Github GraphQL"
          placeholder="Type here at least 3 chars..."
          id="filled-basic"
          variant="filled"
          focused
          style={{ color: "#fff", width: "100%" }}
          onChange={(e) => handleChange(e)}
        />
      </Box>
      {isSearching ? (
        <Box style={{ display: "flex", justifyContent: "center" }}>
          <CircularProgress />
        </Box>
      ) : (
        ""
      )}
      <Box style={{ opacity: 0.7, margin: 5 }}>
        {records
          ? records.map((ele) => (
              <Box key={ele.node.id}>
                <Card sx={{ m: 2 }}>
                  <CardContent>
                    <Typography variant="h5" component="div">
                      {ele.node.name}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                      GitHub
                    </Typography>
                    <Button href={ele.node.url} size="small">
                      Check GitHub Repo
                    </Button>
                  </CardContent>
                </Card>
              </Box>
            ))
          : ""}
      </Box>
    </Container>
  );
};

export default Zad3;
